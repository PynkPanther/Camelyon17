first i suggest you create a new VIRTUAL ENV, because i didnt yet cleanup my freezed requirements.txt.
after that u can isntall the requirements with the following command (not tested yet either):

pip install -r requirements.txt

if that somehow fails you can still try to run the scripts below and install missing packages manually according to the error messages.


##################################################
## [start_demo_process_heatmaps_paddedFalse.sh] ##
##################################################

note: this DEMO-Script uses a small subset of heatmaps in './input_dir/RESUTLS_FROM_JONAS/'.
The original scripts "start_process_heatmaps_paddedFalse_full_dataset.sh" and "start_process_heatmaps_paddedTrue_full_dataset.sh"
both use the complete datasets which are not in this repository !!!

starts process_heatmaps.py with demo arguments:
[threshold]=0.5  ### threshold for binarization
[morphological_opening_iterations]=1
[morphological_closing_iterations]=6
[input_dir]='./input_dir/RESUTLS_FROM_JONAS/' ### this contains a small subset of the heatmaps
[output_dir]='./output_dir/DEMO_paddedFalse_threshold0.5_open1_close6/'
[xml_dir]='./input_dir/xml_masks/'
[padding]=False ### True only for CNN afterwards
[save_pics]=True ### saves heatmaps for all preprocessing steps

- reads in hdf5-files from [input_dir]/RESULTS_FROM_JONAS/
- saves heatmaps into [output_dir]/01_heatmaps/
- saves thresholded heatmaps into [output_dir]/02_heatmaps_threshold/
- saves morphological opened and closed heatmaps into [output_dir]/03_heatmaps_opening_closing/
- saves filled objects heatmaps into .[output_dir]/04_heatmaps_filled_holes/
- and so on
- saves log.txt into [output_dir]/log.txt

- reads in [input_dir]/labels_c16_eval.csv
- reads in [input_dir]/labels_c17_train.csv
- saves labels and extraced features into [output_dir]/features.csv

- reads in XML-Masks from pathologists in [xml_dir]
- creates filled polygons based on these XML-Masks and saves them into [output_dir]/05a_xml_masks_filled_holes/
- subtracts these polygons from polygons created by heatmaps and computes the error
- saves error at the end of [output_dir]/log.txt

##################################################
## [start_train_ffn.sh]                         ##
##################################################

- parameters can be set in the shell-script
- iterates over all DIRs in "./output_dir/" which contain 'paddedFalse' and uses the "features.csv" there
- trains a MultiLayerPerceptron which parameters specified in the shell script on ALL those found "features.csv"-files
- saves a folder in each of the iterated DIRs which contains the parameters in the folder name
- generates a "results.pickle"-file in there
- generates a "predicted_trainset.csv" in there

##################################################
## [start_train_random_forest.sh]               ##
##################################################

- parameters can be set in the shell-script
- iterates over all DIRs in "./output_dir/" which contain 'paddedFalse' and uses the "features.csv" there
- trains a RandomForest which parameters specified in the shell script on ALL those found "features.csv"-files
- save a folder in all those iterated DIRs which contains the parameters in the folder name
- generates a "results.pickle"-file in there
- generates a "predicted_trainset.csv" in there

##################################################
## [start_train_cnn.sh]                         ##
##################################################

- parameters can be set in the shell-script
- iterates over all DIRs in "./output_dir/" which contain 'paddedTrue' and uses the IMAGE-FILES in "01a_heatmaps_gray"
- trains a CNN witch parameters specified in the shell script on ALL those different "./output_dir/paddedTrue*/"01a_heatmaps_gray/"-folders
- save a folder in all those iterated DIRs which contains the parameters in the folder name
- generates a "results.pickle"-file in there
- generates a "predicted_trainset.csv" in there

##################################################
## [start_evaluation.sh]                        ##
##################################################

- searches "./output_dir/*" and subdirs for "resuls.pickle" files and generates images showing the training process
- searches "./output_dir/*" and subdirs for predicted_trainset.csv" and genreates a "kappa.txt" containing the score on the training-set
- generates "./output_dir/final_summary.csv"
