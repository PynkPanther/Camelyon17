#!/bin/bash

for dir in ./output_dir/*/
do
    if [[ "$dir" == *paddedTrue* ]]; then

        echo "$dir"

        ### Feel free experimenting ;)
        examples_to_use_per_label=9999
        cnn_settings=9 # currentyl only 0-8 supported (see train_cnn.py what these settings are)

        ### DONT TOUCH
        img_height=835 #835
        img_width=430 #430
        #main_working_dir='./output_dir/paddedTrue_threshold0.5_open1_close6/'
        main_working_dir="$dir"
        input_dir="$main_working_dir"01a_heatmaps_gray/
        csv_labels="$main_working_dir"features.csv
        output_dir="$main_working_dir"cnn_settings"$cnn_settings"_examplesperlabel"$examples_to_use_per_label"/
        mkdir "$output_dir"
        log="$output_dir"/log.txt


        python3 train_cnn.py "$input_dir" "$csv_labels" "$output_dir" "$img_width" "$img_height" "$examples_to_use_per_label" "$cnn_settings" 2>&1 | tee "$log"
        #python3 train_cnn.py "$input_dir" "$csv_labels" "$output_dir" "$img_width" "$img_height" "$examples_to_use_per_label" "$cnn_settings"
    fi
done

