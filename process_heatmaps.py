import sys
from my_libs.heatmapsUtil import *
from my_libs.csvUtil import *


def check_program_args(args):
    if len(args) != 13:
        print('This program must be started with 11 arguments in the following order:')
        print('(1): input.directory: path to dir with subdirs, each containing one hdf5-file containing \'dataset\' 3d-numpy-array')
        print('(2): output-directory')
        print('(3): cropping and padding of images (important for CNNs):')
        print('\t True: images will be cropped (lines, rows at the border containing only 0s.')
        print('\t afterwards all images will be padded to maximum height, width with 0s again.')
        print('(4): threshold for binarization of the heatmaps (0.0-1.0, 0.5-0.8 suggested)')
        print('(5): times to run morphological opening after binarization (0-2 suggested)')
        print('(6): times to run morphological closing after opening (4-8 suggested)')
        print('(7): path to camelyon16 stage labels csv.')
        print('(8): path to camelyon17 stage labels csv.')
        print('(9): path to directory with xml-maks for tumor containing slided.')
        print('(10): if arg#3 == True: width to pad images. If 0, then calculating best value (longer runtime)')
        print('(11): if arg#3 == True: height to pad images. If 0, then calculating best value (longer runtime)')
        print('(12): save heatmaps to output-directory (all preprocessing steps) (True/False)')


if __name__ == '__main__':

    check_program_args(sys.argv)

    input_dir = sys.argv[1]
    output_dir = sys.argv[2]
    padding = sys.argv[3].lower() == 'true'
    threshold = float(sys.argv[4])
    opening_iter = int(sys.argv[5])
    closing_iter = int(sys.argv[6])

    csv_c16_eval = sys.argv[7]
    csv_c17_train = sys.argv[8]
    xml_masks_dir = sys.argv[9]
    max_y = int(sys.argv[10])
    max_x = int(sys.argv[11])

    save_pics = sys.argv[12].lower() == 'true'

    c16_name_labels, c16_nb_labels = parse_c16_csv(csv_c16_eval)
    c17_name_labels, c17_nb_labels = parse_c17_csv(csv_c17_train)
    total_nb_labels = np.array(c16_nb_labels) + np.array(c17_nb_labels)


    if padding and max_x == 0 and max_y == 0:
        print('start detecting max height and width')
        max_y, max_x = get_max_heigth_width(input_dir=input_dir, pad_mode=padding)


    data_name, ftr_sum_probs, ftr_avg_probs, ftr_highest_prob, ftr_size_total, ftrs_object, ftrs_object2 = extract_features_all_heatmaps(
        input_dir, output_dir, xml_masks_dir,
        threshold, closing_iter, opening_iter,
        pad_to_height=max_y, pad_to_width=max_x,
        pad_mode=padding, save_pics=save_pics)

    data_label = []
    for d in data_name:
        label_found = False
        for l in c16_name_labels:
            if d == l[0]:
                label_found = True
                data_label.append(l[1])
        if not label_found:
            for l in c17_name_labels:
                if d == l[0]:
                    label_found = True
                    data_label.append(l[1])
        if not label_found: ### Normal_xxx / Tumor_xxx -> not in any csv
            if 'Normal' in d:
                data_label.append('negative')
            elif 'Tumor' in d:
                data_label.append('c16_train_tumor')
            elif 'patient' in d:
                data_label.append('c17_test_unknown') ### not tested to work, but loading labels with this label only should pull test set

    f = open(output_dir + '/features.csv', 'w')
    f.write('data_name,label,sum_all_probs, avg_all_probs,highest_prob,all_objects_size,biggest_object_size,biggest_object_major,biggest_object_minor_axis,2nd_biggest_object_size,2nd_biggest_object_major,2nd_biggest_object_minor_axis\n')
    for e in range(len(data_name)):
        f.write('%s,%s,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n'%(data_name[e], data_label[e], ftr_sum_probs[e], ftr_avg_probs[e],
                                             ftr_highest_prob[e], ftr_size_total[e], ftrs_object[e][0],
                                             ftrs_object[e][1], ftrs_object[e][2], ftrs_object2[e][0],
                                             ftrs_object2[e][1], ftrs_object2[e][2]))
    f.close()
