#!/bin/bash

### Feel free experimenting ;)
t_min=4 # threshold binarization minimum value /10 (e.g. 4 -> 0.4)
t_max=4 # threshold binarization maximum value /10 (e.g. 4 -> 0.4)
o_min=3 # how often to perform morphological opening minimum range value
o_max=4 # how often to perform morphological opening maximum range value
c_min=0 # how often to perform morphological closing minimum range value
c_max=7 # how often to perform morphological closing maximum range value

### DONT TOUCH
for (( t=t_min; t<=t_max; t++ )) ;
do
    for (( o=o_min; o<=o_max; o++ )) ;
    do
        for (( c=c_min; c<=c_max; c++ )) ;
        do

            threshold=$(bc <<< 'scale=2; '$t'/10''' )
            closing_iter="$c"
            opening_iter="$o"
            padding=False
            save_pics=False
            pad_y=0 # if padding True pad to height. IF 0, then calculate optimum
            pad_x=0 # if padding True pad to width. IF 0, then calculate optimum

            csv_c16_eval='./input_dir/labels_c16_eval.csv'
            csv_c17_train='./input_dir/labels_c17_train.csv'
            xml_dir='./input_dir/xml_masks/'
            input_dir='../CAMELYON1617_HEATMAPS/'
            output_dir='./output_dir/'
            output_dir="$output_dir"padded"$padding"_threshold"$threshold"_open"$opening_iter"_close"$closing_iter"
            mkdir "$output_dir"
            log="$output_dir"/log.txt


            python3 process_heatmaps.py "$input_dir" "$output_dir"  "$padding" "$threshold" "$opening_iter" "$closing_iter" "$csv_c16_eval" "$csv_c17_train" "$xml_dir" "$pad_y" "$pad_x" "$save_pics" 2>&1 | tee "$log"
        done
    done
done