import pickle
import matplotlib.pyplot as plt
import numpy as np
import sys




def check_program_args(args):
    if len(args) != 3:
        print('This program must be started with 2 arguments in the following order:')
        print('(1): output-directory')
        print('(2): input pickle-file. produced by "train_cnn.py", "train_random_forest.py" and "train_ffn.py"')




if __name__ == '__main__':

    font = {'family': 'normal',
            'weight': 'normal',
            'size': 18}
    plt.rc('font', **font)
    plt.rc('xtick', labelsize=18)
    plt.rc('ytick', labelsize=18)


    check_program_args(sys.argv)

    output_dir = sys.argv[1]
    input_file = sys.argv[2]
    out_acc_file = output_dir + 'accuracy.png'
    out_f1train_file = output_dir + 'f1train.png'
    out_f1test_file = output_dir + 'f1test.png'
    acc_y_min = 0.
    acc_y_max = 1.
    f1_y_min = 0.
    f1_y_max = 1.


    with open(input_file, 'rb') as f:
        epoch_accs_train, epoch_accs_test, epoch_f1scores_train, epoch_f1scores_test = pickle.load(f)

    f1_train_array = np.array(epoch_f1scores_train)
    f1_test_array = np.array(epoch_f1scores_test)

    print('%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f' %(epoch_accs_train[-1], epoch_accs_test[-1], f1_train_array[-1][0], f1_train_array[-1][1], f1_train_array[-1][2], f1_train_array[-1][3], f1_test_array[-1][0], f1_test_array[-1][1], f1_test_array[-1][2], f1_test_array[-1][3]))

    if len(epoch_accs_train) == 1:
        epoch_accs_train = np.concatenate((epoch_accs_train, epoch_accs_train))
        epoch_accs_test = np.concatenate((epoch_accs_test, epoch_accs_test))
        f1_train_array = np.concatenate((f1_train_array, f1_train_array))
        f1_test_array = np.concatenate((f1_test_array, f1_test_array))
    else:
        range_x = range(len(epoch_accs_train))

    acc_y_min = 0.0
    acc_y_max = 1.1
    f1_y_min = 0.0
    f1_y_max = 1.1

    ############# Train / Test Accuracy #############
    plt.plot(range(len(epoch_accs_train)), epoch_accs_train, 'b', label='train acc', linewidth=3.0)
    plt.plot(range(len(epoch_accs_test)), epoch_accs_test, 'r', label='valid acc',linewidth=3.0)
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(loc='lower right')
    axes = plt.gca()
    # axes.set_xlim([xmin,xmax])
    axes.set_ylim([acc_y_min, acc_y_max])
    plt.savefig(out_acc_file, bbox_inches='tight')
    plt.close()

    ############# Train F1 Score #############
    for e in range(len(f1_train_array[0])):
        plt.plot(range(len(f1_train_array[:,e])), f1_train_array[:,e], label='f1 train label ' + str(e),linewidth=3.0)
    plt.ylabel('f1 score')
    plt.xlabel('epoch')
    plt.legend(loc='lower right')
    axes = plt.gca()
    # axes.set_xlim([xmin,xmax])
    axes.set_ylim([f1_y_min, f1_y_max])
    plt.savefig(out_f1train_file, bbox_inches='tight')
    plt.close()


    ############# Test F1 Score #############
    for e in range(len(f1_test_array[0])):
        plt.plot(range(len(f1_test_array[:,e])), f1_test_array[:,e], label='f1 valid label ' + str(e),linewidth=3.0)
    plt.ylabel('f1 score')
    plt.xlabel('epoch')
    plt.legend(loc='lower right')
    axes = plt.gca()
    # axes.set_xlim([xmin,xmax])
    axes.set_ylim([f1_y_min, f1_y_max])
    plt.savefig(out_f1test_file, bbox_inches='tight')
    plt.close()




