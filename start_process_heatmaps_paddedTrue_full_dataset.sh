#!/bin/bash

### Feel free experimenting ;)
threshold=0.5
closing_iter=5
opening_iter=0
padding=True
save_pics=False
pad_y=0 # if padding True pad to height. IF 0, then calculate optimum
pad_x=0 # if padding True pad to width. IF 0, then calculate optimum

### DONT TOUCH
csv_c16_eval='./input_dir/labels_c16_eval.csv'
csv_c17_train='./input_dir/labels_c17_train.csv'
xml_dir='./input_dir/xml_masks/'
input_dir='../CAMELYON1617_HEATMAPS/'
output_dir='./output_dir/'
output_dir="$output_dir"padded"$padding"_threshold"$threshold"_open"$opening_iter"_close"$closing_iter"
mkdir "$output_dir"
log="$output_dir"/log.txt


python3 process_heatmaps.py "$input_dir" "$output_dir"  "$padding" "$threshold" "$opening_iter" "$closing_iter" "$csv_c16_eval" "$csv_c17_train" "$xml_dir" "$pad_y" "$pad_x" "$save_pics" 2>&1 | tee "$log"