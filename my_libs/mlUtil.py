import tensorflow as tf
import numpy as np
import os
import random
import csv

from keras import backend as K
from keras.layers import Dense, Dropout, Conv2D, MaxPool2D, Flatten
from sklearn.metrics import confusion_matrix
from sklearn.metrics import f1_score
from tensorflow.python.ops import image_ops
from keras.utils.np_utils import to_categorical
from sklearn.metrics import accuracy_score





####################################
####        cyclic mode        #####
####################################
# here are three of four layers of the paper Exploiting Cyclic Symmetry in Convolutional Neural Networks [Sander Dielemann] implemented.
# you can find the paper at arxiv: https://arxiv.org/pdf/1602.02660.pdf

### batch_size * 4
def slice_layer(X):
    #r1 = tf.map_fn(lambda img: image_ops.rot90(img, 1), X)
    #r2 = tf.map_fn(lambda img: image_ops.rot90(img, 2), X)
    #r3 = tf.map_fn(lambda img: image_ops.rot90(img, 3), X)
    r1 = tf.map_fn(lambda img: image_ops.flip_left_right(img), X)
    r2 = tf.map_fn(lambda img: image_ops.flip_up_down(img), X)
    r3 = tf.map_fn(lambda img: image_ops.flip_up_down(img), r1)
    concatenated = tf.concat([X,r1,r2,r3],axis=0)
    shape = X.get_shape().as_list()
    return tf.reshape(concatenated,[shape[0]*4,shape[1],shape[2],shape[3]])

### batch_size / 4
def dense_pooling(X, pool_op=tf.reduce_mean, p=4):
    # pool_op has to be a tf reduce operator
    # no spatial structure -> no backrotation (r^-1) needed
    shape = tf.shape(X)
    shape_l = X.get_shape().as_list()
    X_ = tf.reshape(X, (p, shape[0]//p, shape[1]))
    X_ = pool_op(X_, axis=0)
    X_ = tf.reshape(X_, (shape[0]//p, shape_l[1]))
    return X_

### batch_size / 4, feature_size * 4
def stack_dense(X, shift=0, p=4):
    per = np.roll(range(p), -shift)
    X_ = [X[per[i]] for i in range(p) ]
    return tf.concat(X_, axis=1)

### feature_size * 4
def roll_dense(X, p=4):
    shape = tf.shape(X)
    shape_l = X.get_shape().as_list()
    X_ = tf.reshape(X, (p, shape[0]//p, shape[1]))
    X_concat = [stack_dense(X_, i, p) for i in range(p) ]
    X_concat = tf.concat([X_concat], axis=0)
    return tf.reshape(X_concat,[-1,shape_l[1]*4])


def build_model_keras(input, n_filters_convs=[96, 128, 192, 192, 128],
                      filter_sizes=[3, 3, 3, 3, 3],
                      conv_strides=[1, 1, 1, 1, 1],
                      pooling=[1, 1, 0, 0, 1],
                      pool_size=2,
                      pool_stride=2,
                      droput_conv=0.25,
                      n_filters_lin=[256, 256],
                      dropout_lin=0.5,
                      n_labels=10,
                      cyclic_mode=False,
                      roll_dense=False):

    print('##########################################')
    print('##        Network Architecture          ##')
    print('##########################################')

    x = input

    if len(n_filters_convs) > 0:

        if cyclic_mode:
            print("--slice")
            print('(shape b4: %s)' % str(x.get_shape()))
            x = slice_layer(x)
            print('(shape after: %s)' % str(x.get_shape()))

        for i, n_output in enumerate(n_filters_convs):
            print('--conv [%d@%dx%d, stride_%d]' %(n_filters_convs[i],filter_sizes[i],filter_sizes[i],conv_strides[i]))
            x = Conv2D(n_filters_convs[i], kernel_size=(filter_sizes[i], filter_sizes[i]),
                       activation='relu', padding='same',
                       strides=(conv_strides[i], conv_strides[i]))(x)
            print('--conv_dropout [%f]' %droput_conv)
            x = Dropout(droput_conv)(x)
            if pooling[i] == 1:
                print('--maxpool [%dx%d, stride_%d]' %(pool_size, pool_size, pool_stride))
                x = MaxPool2D(pool_size=(pool_size, pool_size), padding='same', strides=(pool_stride, pool_stride))(x)

        #print('--conv_dropout [%f]' %droput_conv)
        #x = Dropout(droput_conv)(x)

        x = Flatten()(x)

    for i, n_output in enumerate(n_filters_lin):
        print('--dense [%d]' %n_filters_lin[i])
        x = Dense(n_filters_lin[i], activation='relu')(x)
        print('--dense dropout [%f]' % dropout_lin)
        x = Dropout(dropout_lin)(x)
        if i == 0 and cyclic_mode and roll_dense:
            print("--dense_roll)")
            print('(shape b4: %s' % str(x.get_shape()))
            x = roll_dense(x)
            print('(shape after: %s)' % str(x.get_shape()))

    x2 = x

    print('--softmax [n_labels_%d]' %n_labels)
    x2 = Dense(n_labels, activation='softmax')(x2)

    if cyclic_mode:
        print("--dense_pooling")
        print('(shape b4: %s)' % str(x.get_shape()))
        x2 = dense_pooling(x2, pool_op=tf.reduce_mean, p=4)
        # x2 = dense_pooling(x2, pool_op=tf.reduce_max, p=4)
        print('(shape after: %s)' % str(x.get_shape()))

    preds = x2

    return x, preds


def eval_rf(x, y, random_forest):
    y_predict = random_forest.predict(x)
    acc = accuracy_score(y, y_predict)
    f1scores = f1_score(y, y_predict, average=None)
    cm = confusion_matrix(y, y_predict)
    return acc, cm, f1scores, y_predict


def remove_samples_from_set(x_to_remove_from, names_to_remove_from, names_to_remove):
    idx_to_del = []
    for i in range(len(names_to_remove_from)):
        if names_to_remove_from[i] in names_to_remove:
            idx_to_del.append(i)
    names_to_remove_from = np.delete(names_to_remove_from, idx_to_del, axis=0)
    x_to_remove_from = np.delete(x_to_remove_from, idx_to_del, axis=0)
    return x_to_remove_from, names_to_remove_from


def remove_duplicates(test_preds_all, test_names_all):
    duplicates_found = True
    while duplicates_found:
        duplicates_found = False
        idxs_to_remove = []
        for e in range(len(test_names_all)):
            c = test_names_all.count(test_names_all[e])
            if c > 1:
                duplicates_found = True
                idxs_to_remove.append(e)
        for index in sorted(idxs_to_remove, reverse=True):
            del(test_names_all[index])
            del(test_preds_all[index])
    return test_preds_all, test_names_all


def get_train_test_split(dirs, percent_train, seed, max_examples):
    filenames = []
    for e in range(0, len(dirs)):
        filenames.append([os.path.join(dirs[e], fname)
                          for fname in os.listdir(dirs[e])][:])

    for e in range(0, len(dirs)):
        if max_examples[e] == 0:
            max_examples[e] = len((filenames[e]))

    print(len(filenames[0]))

    random.seed(seed)
    train_l = []
    test_l = []
    train_all = []
    test_all = []
    labels_train_all = np.empty(0)
    labels_test_all = np.empty(0)
    for e in range(0, len(dirs)):
        random.shuffle(filenames[e])
        filenames[e] = filenames[e][:max_examples[e]]
        train_l.append(filenames[e][:int(len(filenames[e]) * percent_train)])
        train_all += train_l[e]
        test_l.append(filenames[e][int(len(filenames[e]) * percent_train):])
        test_all += test_l[e]
        labels_train_all = np.concatenate((labels_train_all, np.full(len(train_l[e]), e, dtype=np.int32)))
        labels_test_all = np.concatenate((labels_test_all, np.full(len(test_l[e]), e, dtype=np.int32)))
        print('#label %d in train: %d' % (e, len(train_l[e])))
        print('#label %d in test: %d' % (e, len(test_l[e])))
    print('# examples in train: %d' % len(train_all))
    print('# examples in test %d' % len(test_all))

    return train_all, test_all, labels_train_all, labels_test_all


def get_camelyon_train_test_split_cnn_old(dir_imgs, percent_train, fold, max_examples, csv_file, labels_to_use):
    print('##########################################')
    print('##        Train / Test - Split          ##')
    print('##########################################')
    filenames = []
    filenames += ([os.path.join(dir_imgs, fname)
                   for fname in os.listdir(dir_imgs)][:])

    for e in range(0, len(max_examples)):
        if max_examples[e] == 0:
            max_examples[e] = len((filenames))

    labels = []
    data_l = []
    for l in labels_to_use:
        data_l.append([])
    for f in filenames:
        fname = f.replace(dir_imgs, '')
        fname = fname.replace('.png', '')
        label_found = False
        with open(csv_file) as csvfile:
            reader1 = csv.reader(csvfile, delimiter=',', quotechar='|')
            for row in reader1:
                if fname in row[0] and not label_found:
                    for e in range(len(labels_to_use)):
                        if labels_to_use[e] in row[1]:
                            labels.append(e)
                            label_found = True
        if label_found:
            data_l[labels[-1]].append(f)

    shuffle_lists_the_same(data_l, 5)

    train_all = []
    test_all = []
    labels_train_all = np.empty(0)
    labels_test_all = np.empty(0)
    for e in range(len(data_l)):
        data_l[e] = data_l[e][:max_examples[e]]
        train_tmp = data_l[e][:int(percent_train * len(data_l[e]))]
        test_tmp = data_l[e][int(percent_train * len(data_l[e])):]
        train_all += train_tmp
        test_all += test_tmp
        labels_train_all = np.concatenate((labels_train_all, np.full(len(train_tmp), e, dtype=np.int32)))
        labels_test_all = np.concatenate((labels_test_all, np.full(len(test_tmp), e, dtype=np.int32)))
        print('label #%d examples in train / test: [%d/%d]' % (e, len(train_tmp), len(test_tmp)))
    print('# examples in train: %d' % len(train_all))
    print('# examples in test %d' % len(test_all))
    ### shuffle all returned data
    shuffle_lists_the_same([train_all, labels_train_all, test_all, labels_test_all], fold)

    return train_all, test_all, labels_train_all, labels_test_all


def get_camelyon_train_test_split_cnn(dir_imgs, percent_train, fold, max_examples, csv_file, labels_to_use):
    print('##########################################')
    print('##        Train / Test - Split          ##')
    print('##########################################')
    filenames = []
    filenames += ([os.path.join(dir_imgs, fname)
                   for fname in os.listdir(dir_imgs)][:])

    for e in range(0, len(max_examples)):
        if max_examples[e] == 0:
            max_examples[e] = len((filenames))

    labels = []
    data_l = []
    for l in labels_to_use:
        data_l.append([])
    for f in filenames:
        fname = f.replace(dir_imgs, '')
        fname = fname.replace('.png', '')
        label_found = False
        with open(csv_file) as csvfile:
            reader1 = csv.reader(csvfile, delimiter=',', quotechar='|')
            for row in reader1:
                if fname in row[0] and not label_found:
                    for e in range(len(labels_to_use)):
                        if labels_to_use[e] in row[1]:
                            labels.append(e)
                            label_found = True
        if label_found:
            data_l[labels[-1]].append(f)

    shuffle_lists_the_same(data_l, 5)

    ### get max_examples per label
    train_all = []
    test_all = []
    labels_train_all = np.empty(0)
    labels_test_all = np.empty(0)

    for e in range(len(data_l)):
        train_ftrs_tmp, test_ftrs_tmp = divide_list_into_splits(data_l[e], fold, percent_train, max_examples[e])
        train_all += train_ftrs_tmp
        test_all += test_ftrs_tmp
        labels_train_all = np.concatenate((labels_train_all, np.full(len(train_ftrs_tmp), e, dtype=np.int32)))
        labels_test_all = np.concatenate((labels_test_all, np.full(len(test_ftrs_tmp), e, dtype=np.int32)))
        print('label #%d examples in train / test: [%d/%d]' % (e, len(train_ftrs_tmp), len(test_ftrs_tmp)))
    print('# examples in train: %d' % len(train_all))
    print('# examples in test %d' % len(test_all))

    ### shuffle all returned data
    shuffle_lists_the_same([train_all, labels_train_all, test_all, labels_test_all], fold)

    return train_all, test_all, labels_train_all, labels_test_all


def shuffle_lists_the_same(list_of_lists, seed):
    for l in list_of_lists:
        np.random.seed(seed)
        np.random.shuffle(l)
    return list_of_lists




def get_camelyon_train_test_split_ffn(percent_train, fold, max_examples, csv_file, labels_to_use, features_to_use):
    print('##########################################')
    print('##        Train / Test - Split          ##')
    print('##########################################')

    ftr_vectors = []
    for l in labels_to_use:
        ftr_vectors.append([])

    with open(csv_file) as csvfile:
        reader1 = csv.reader(csvfile, delimiter=',', quotechar='|')
        line_counter = 0
        for row in reader1:
            if line_counter > 0:
                #if 'Normal' not in row[0] and 'Tumor' not in row[0]:
                #    if not ('Test' in row[0] and 'negative' in row[1]):
                if 'Tumor' not in row[0]:
                    if True:
                        ftr_v = []
                        for l in enumerate(labels_to_use):
                            if l[1] == row[1]:
                                ftr_v.append(row[0]) # name
                                for e in range(len(row)):
                                    if e in features_to_use:
                                        ftr_v.append(row[e])
                                ftr_vectors[l[0]].append(ftr_v)
            line_counter += 1

    shuffle_lists_the_same(ftr_vectors, 5)

    ### get max_examples per label
    train_all = []
    test_all = []
    labels_train_all = np.empty(0)
    labels_test_all = np.empty(0)

    for e in range(len(ftr_vectors)):
        train_ftrs_tmp, test_ftrs_tmp = divide_list_into_splits(ftr_vectors[e], fold, percent_train, max_examples[e])
        train_all += train_ftrs_tmp
        test_all += test_ftrs_tmp
        labels_train_all = np.concatenate((labels_train_all, np.full(len(train_ftrs_tmp), e, dtype=np.int32)))
        labels_test_all = np.concatenate((labels_test_all, np.full(len(test_ftrs_tmp), e, dtype=np.int32)))
        print('label #%d examples in train / test: [%d/%d]' % (e, len(train_ftrs_tmp), len(test_ftrs_tmp)))
    print('# examples in train: %d' % len(train_all))
    print('# examples in test %d' % len(test_all))

    ### shuffle all returned data
    shuffle_lists_the_same([train_all, labels_train_all, test_all, labels_test_all], fold)

    train = np.array(train_all)[:,1:] # [:,1:] is label attribute
    train_names = np.array(train_all)[:,0]
    if percent_train < 1.0:
        test_names = np.array(test_all)[:,0]
        test = np.array(test_all)[:,1:]
    else:
        test_names = np.empty(0)
        test = np.empty(0)
    return train, test, labels_train_all, labels_test_all, train_names, test_names


def remove_bad_training_samples(x_train, y_train, names_train):
    bad_samples = ['patient_035_node_2','patient_013_node_2','patient_037_node_4','patient_084_node_4','patient_088_node_4','patient_089_node_3']
    idxs_to_remove = []
    for e in range(len(names_train)):
        if names_train[e] in bad_samples:
            idxs_to_remove.append(e)
    x_train = np.delete(x_train, idxs_to_remove, axis=0)
    y_train = np.delete(y_train, idxs_to_remove, axis=0)
    names_train = np.delete(names_train, idxs_to_remove, axis=0)
    return  x_train, y_train, names_train


def divide_list_into_splits(list, fold, percent_train, max_examples):
    list = list[:max_examples]
    s1 = round(fold * len(list) * (1. - percent_train))
    e1 = round((fold + 1) * len(list) * (1. - percent_train))
    #print(s1, e1)
    split2 = list[s1:e1]
    split1 = list[:s1] + list[e1:]
    return split1, split2



def get_feature_vectors_and_labels(split_percentage, fold, max_examples_to_use, csv_labels, labels_to_use, features_to_use, one_hot=True):
    train_vectors, test_vectors, labels_train, labels_test, train_names, test_names = get_camelyon_train_test_split_ffn(
        split_percentage, fold, max_examples_to_use,
        csv_labels, labels_to_use, features_to_use)
    train_data = np.array(train_vectors, dtype=np.float32)
    test_data = np.array(test_vectors, dtype=np.float32)

    if one_hot:
        labels_train = to_categorical(labels_train, num_classes=len(labels_to_use))
        labels_test = to_categorical(labels_test, num_classes=len(labels_to_use))
    return train_data, test_data, labels_train, labels_test, train_names, test_names


def normalize_vectors(vector1, vector2=None, vector3=None):
    ####### normalization #######
    for e in range(len(vector1[0])):
        max_v1 = np.amax(vector1[:, e])
        max_abs = max_v1
        if vector2 is not None:
            max_v2 = np.amax(vector2[:, e])
            max_abs = max_v1 if max_v1 > max_v2 else max_v2
        if vector3 is not None:
            max_v2 = np.amax(vector3[:, e])
            max_abs = max_v1 if max_v1 > max_v2 else max_v2

        if max_abs == 0: max_abs = 1
        vector1[:, e] = vector1[:, e] / max_abs
        if vector2 is not None:
            vector2[:, e] = vector2[:, e] / max_abs
        if vector3 is not None:
            vector3[:, e] = vector3[:, e] / max_abs
    return vector1, vector2, vector3


def get_queue_batches_cnn(filenames_all, labels_all, height, width, n_channels, n_labels, batch_size):
    filenames_all_tensor = tf.convert_to_tensor(filenames_all, dtype=tf.string)
    labels_all_tensor = tf.convert_to_tensor(labels_all, dtype=tf.int32)
    queue = tf.train.slice_input_producer(
        [filenames_all_tensor, labels_all_tensor],
        shuffle=False)

    image_file = tf.read_file(queue[0])
    image = tf.image.decode_png(image_file, channels=n_channels)
    image.set_shape((height, width, n_channels))
    image = tf.reshape(image, [height * width * n_channels])
    image = tf.cast(image, tf.float32) / 255.
    label = queue[1]
    label = tf.convert_to_tensor(label, dtype=tf.int32)
    label = tf.one_hot(indices=label, depth=n_labels, dtype=tf.float64)

    min_after_dequeue = batch_size * 2
    capacity = batch_size * 4

    data, labels, name = tf.train.batch(
        [image, label, queue[0]],
        batch_size=batch_size,
        num_threads=6,
        capacity=capacity,
        allow_smaller_final_batch=True)

    #data, labels, name = tf.train.shuffle_batch(
    #    [image, label, queue[0]],
    #    batch_size=batch_size,
    #    num_threads=6,
    #    capacity=capacity,
    #    min_after_dequeue=min_after_dequeue,
    #    allow_smaller_final_batch=True)

    return data, labels, name


def get_rest_of_ftr_vectors(max_examples_to_use, csv_file, labels_to_use, features_to_use, one_hot):
    x_train, _, y_train, _, names_train, _ = get_feature_vectors_and_labels(1.0, 0,
                                                                           max_examples_to_use,
                                                                           csv_file, labels_to_use,
                                                                           features_to_use,
                                                                           one_hot=one_hot)
    max_examples_to_use2 = [i * 999999 for i in max_examples_to_use]
    x_rest, _, y_rest, _, names_rest, _ = get_feature_vectors_and_labels(1.0, 0,
                                                                           max_examples_to_use2,
                                                                           csv_file, labels_to_use,
                                                                           features_to_use,
                                                                           one_hot=one_hot)
    x_train, x_test, _ = normalize_vectors(x_train, x_rest)
    x_rest, _ = remove_samples_from_set(x_rest, names_rest.copy(), names_train)
    y_rest, names_rest = remove_samples_from_set(y_rest, names_rest, names_train)
    return x_train, y_train, names_train, x_rest, y_rest, names_rest


def write_predictions_to_file(pat_s, pat_e, f_name, preds, preds_names, labels_to_use, zfill=3):
    f = open(f_name, 'w')
    f.write('patient,stage\n')
    for p in range(pat_s, pat_e):
        pN_str = 'patient_' + str(p).zfill(3) + '.zip'
        for s in range(2):
            if s == 1:
                pN = '!!!!!!!!!!'
                if labels[0] == 5:
                    pN = 'pN0'
                elif labels[0] + labels[1] == 5:
                    pN = 'pN0(i+)'
                elif labels[0] + labels[1] + labels[2] == 5:
                    pN = 'pN1mi'
                elif labels[2] + labels[3] >= 1 and labels[2] + labels[3] < 4:
                    pN = 'pN1'
                elif labels[2] + labels[3] >= 4:
                    pN = 'pN2'
                f.write('%s,%s\n' % (pN_str, pN))
            else:
                labels = [0, 0, 0, 0]
            for n in range(5):
                pat_str = 'patient_' + str(p).zfill(zfill) + '_node_' + str(n)
                patient_found = False
                for r in range(len(preds_names)):
                    if pat_str in preds_names[r]:
                        patient_found = True
                        if s == 0:
                            labels[int(preds[r])] += 1
                        else:
                            f.write('%s,%s\n' % (pat_str + '.tif', labels_to_use[int(preds[r])]))
                if not patient_found:
                    if s == 0:
                        labels[0] += 1
                    else:
                        f.write('%s,%s\n' % (pat_str + '.tif', labels_to_use[0]))
                        #f.write('___DEBUG___')
    f.close()


def print_progress(epoch, epoch_accs_train, epoch_accs_test, cm_train, cm_test, f1_train, f1_test, phase=''):
    try:
        print('######## epoch [%d] - %s ########' %(epoch,phase))
        if len(f1_train[-1]) == 3 and len(f1_test[-1]) == 3:
            print('#### TRAIN ####')
            print('accuracry - f1_score: \t %.2f  -  %.2f/%.2f/%.2f' %(epoch_accs_train[-1],f1_train[-1][0], f1_train[-1][1], f1_train[-1][2]))
            print(cm_train)
            print('#### TEST ####')
            print('accuracry - f1_score: \t %.2f  -  %.2f/%.2f/%.2f' %(epoch_accs_test[-1],f1_test[-1][0], f1_test[-1][1], f1_test[-1][2]))
            print(cm_test)
        elif len(f1_train[-1]) == 4 and len(f1_test[-1]) == 4:
            print('#### TRAIN ####')
            print('accuracry - f1_score: \t %.2f  -  %.2f/%.2f/%.2f/%.2f' %(epoch_accs_train[-1],f1_train[-1][0], f1_train[-1][1], f1_train[-1][2], f1_train[-1][3]))
            print(cm_train)
            print('#### TEST ####')
            print('accuracry - f1_score: \t %.2f  -  %.2f/%.2f/%.2f/%.2f' %(epoch_accs_test[-1],f1_test[-1][0], f1_test[-1][1], f1_test[-1][2], f1_test[-1][3]))
            print(cm_test)
        elif len(f1_train[-1]) == 2 and len(f1_test[-1]) == 2:
            print('#### TRAIN ####')
            print('accuracry - f1_score: \t %.2f  -  %.2f/%.2f' % (epoch_accs_train[-1], f1_train[-1][0], f1_train[-1][1]))
            print(cm_train)
            print('#### TEST ####')
            print('accuracry - f1_score: \t %.2f  -  %.2f/%.2f' % (epoch_accs_test[-1], f1_test[-1][0], f1_test[-1][1]))
            print(cm_test)
        else:
            print('#### TRAIN ####')
            print('accuracry - f1_score: \t %.2f ' %(epoch_accs_train[-1]))
            print(f1_train[-1])
            print(cm_train)
            print('#### TEST ####')
            print('accuracry - f1_score: \t %.2f ' %(epoch_accs_test[-1]))
            print(f1_test[-1])
            print(cm_test)
    except:
        print('___DEBUG: could not print evaluation')


def eval_split_cnn(preds, acc_value, img, labels, batch_size, sess, in_data, in_labels, in_names, nb_examples):
        processed_imgs = 0
        accs = []
        all_labels_fetched = np.empty(0)
        all_labels_pred = np.empty(0)
        all_names_pred = np.empty(0)
        while processed_imgs < nb_examples:
            imgs_fetched, labels_fetched, names_fetched = sess.run([in_data, in_labels, in_names])
            norm_batch = imgs_fetched
            labels_pred, acc_t = sess.run([preds, acc_value], feed_dict={img: norm_batch,
                          labels: labels_fetched,
                          K.learning_phase(): 0})
            accs.append(acc_t.sum() / acc_t.size)
            processed_imgs += batch_size
            all_labels_fetched = np.concatenate((all_labels_fetched, np.argmax(labels_fetched,1)))
            all_labels_pred = np.concatenate((all_labels_pred, np.argmax(labels_pred,1)))
            all_names_pred = np.concatenate((all_names_pred, names_fetched))
        f1scores = f1_score(all_labels_fetched, all_labels_pred, average=None)
        #### was a bad workaround making f1-scores faulty
        #try: ### workaround if all data was in crossvalid:
        #    n_f1_score = len(labels_fetched[0])
        #    if len(f1scores) != n_f1_score:
        #        f1scores = [0. for e in range(n_f1_score)]
        #except:
        #    f1scores = [0. for e in range(4)]
        if len(f1scores) != 4:
            f1scores_ill = True
        else:
            f1scores_ill = False
        cm = confusion_matrix(all_labels_fetched,all_labels_pred)
        return accs, cm, f1scores, all_labels_pred, all_names_pred, f1scores_ill


def get_camelyon_labels_n_max_examples(n_labels, max_examples_to_use_per_label):
    if n_labels == 3:
        labels_to_use = ['negative', 'micro', 'macro']
        max_examples_to_use = [max_examples_to_use_per_label,
                               max_examples_to_use_per_label,
                               max_examples_to_use_per_label]
    elif n_labels == 4:
        labels_to_use = ['negative', 'itc', 'micro', 'macro']
        max_examples_to_use = [max_examples_to_use_per_label,
                               max_examples_to_use_per_label,
                               max_examples_to_use_per_label,
                               max_examples_to_use_per_label]
    elif n_labels == 2:
        labels_to_use = ['negative', 'unknown']
        max_examples_to_use = [max_examples_to_use_per_label,
                               max_examples_to_use_per_label]
    else:
        raise Exception('argument #4 must be 2, 3 or 4 labels to use')
    return labels_to_use, max_examples_to_use


def print_average_results(train_acc_avg, test_acc_avg, train_f1_avg, test_f1_avg):
    print('######## AVERAGE CROSS VALID #########')
    print('TRAIN:')
    print('accuracy:',train_acc_avg)
    print('f1:',train_f1_avg)
    print('TEST:')
    print('accuracy:',test_acc_avg)
    print('f1:',test_f1_avg)