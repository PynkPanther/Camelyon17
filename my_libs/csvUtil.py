import csv


def parse_c17_csv(input_file):
    """
    Parses the stage labels csv-file from camelyon17 challenge.

    Parameters
    ----------
    arg1 : str
        Path to the csv-file

    Returns
    -------
    Tuple
        Tuple in the form of (x,(x,x,x,x)):
        (str(names_and_labels), int(#negative, #itc, #micro, #macro)):
        names_and_labels example: "patient_000_node_0,itc"
    """
    neg_c17 = 0
    itc_c17 = 0
    mic_c17 = 0
    mac_c17 = 0
    name_label = []
    with open(input_file) as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in spamreader:
            if '' in row[0]:
                if 'macro' in row[1]:
                    mac_c17 += 1
                    name_label.append((row[0].replace('.tif',''),'macro'))
                elif 'micro' in row[1]:
                    mic_c17 += 1
                    name_label.append((row[0].replace('.tif',''),'micro'))
                elif 'itc' in row[1]:
                    itc_c17 += 1
                    name_label.append((row[0].replace('.tif',''),'itc'))
                elif 'negative' in row[1]:
                    neg_c17 += 1
                    name_label.append((row[0].replace('.tif',''),'negative'))
    return name_label, (neg_c17,itc_c17,mic_c17,mac_c17)


def parse_c16_csv(input_file):
    """
    Parses the stage labels csv-file from camelyon16 challenge.

    Parameters
    ----------
    arg1 : str
        Path to the csv-file

    Returns
    -------
    Tuple
        Tuple in the form of (x,(x,x,x)):
        (str(names_and_labels), int(#negative, #micro, #macro)):
        names_and_labels example: "patient_000_node_0,itc"
    """
    neg_c16 = 0
    itc_c16 = 0
    mic_c16 = 0
    mac_c16 = 0
    name_label = []
    with open(input_file) as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in spamreader:
            # print(row[0], row[3])
            # if 'Test_001' in row[0]:
            if '' in row[0]:
                if 'Macro' in row[3]:
                    mac_c16 += 1
                    name_label.append((row[0],'macro'))
                elif 'Micro' in row[3]:
                    mic_c16 += 1
                    name_label.append((row[0],'micro'))
                elif 'None' in row[3]:
                    neg_c16 += 1
                    name_label.append((row[0],'negative'))
    return name_label, (neg_c16,itc_c16,mic_c16,mac_c16)
