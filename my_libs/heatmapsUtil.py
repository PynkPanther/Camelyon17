import os
import os.path
import numpy as np
import h5py
import matplotlib.pyplot as plt
import sys
from scipy import ndimage
from skimage.measure import regionprops
from PIL import Image, ImageDraw

from .xmlUtil import parse_single_xml_mask



def get_y_x_dims(np_array):
    x_dim = np_array.shape[1]
    y_dim = np_array.shape[0]
    return y_dim, x_dim


def cut_off_zero_sides(img_array_2d):
    a = img_array_2d
    while a[0, :].sum() == 0:
        a = a[1:, :]
    while a[-1, :].sum() == 0:
        a = a[:-1, :]
    while a[:, 0].sum() == 0:
        a = a[:, 1:]
    while a[:, -1].sum() == 0:
        a = a[:, :-1]
    return a


def normalize_image_dims(img_array_2d, desired_x, desired_y):
    np_array = img_array_2d
    x_dim = np_array.shape[1]
    y_dim = np_array.shape[0]
    x_to_pad = (desired_x - x_dim) // 2
    y_to_pad = (desired_y - y_dim) // 2
    np_array = np.pad(np_array, ((y_to_pad, y_to_pad), (x_to_pad, x_to_pad)), 'constant')
    if np_array.shape[0] != desired_y:
        np_array = np.pad(np_array, ((1, 0), (0, 0)), 'constant')
    if np_array.shape[1] != desired_x:
        np_array = np.pad(np_array, ((0, 0), (1, 0)), 'constant')
    return np_array


def check_min_max_dims(img_array_2d, max_x, min_x, max_y, min_y):
    np_array = img_array_2d
    x_dim = np_array.shape[1]
    y_dim = np_array.shape[0]
    max_x = x_dim if x_dim > max_x else max_x
    min_x = x_dim if x_dim < min_x else min_x
    max_y = y_dim if y_dim > max_y else max_y
    min_y = y_dim if y_dim < min_y else min_y
    return max_x, min_x, max_y, min_y


def extract_features_on_raw_heatmap(img_array_2d):
    ftr_sum_probs = img_array_2d.sum()
    ftr_avg_probs = img_array_2d.sum() / np.count_nonzero(img_array_2d)
    ftr_highest_prob = np.max(img_array_2d)
    return ftr_sum_probs, ftr_avg_probs, ftr_highest_prob


def extract_features_on_closed_heatmap(img_array_2d, plot_mode=False):
    ftr_size_total = 0
    ftr_size_biggest_objects = []

    label_im, nb_labels = ndimage.label(img_array_2d)

    if plot_mode:
        plt.figure(figsize=(6, 30))
        plt.subplot(221)
        plt.imshow(img_array_2d)
        plt.subplot(222)
        plt.imshow(label_im)
        plt.show()

    sizes = ndimage.sum(img_array_2d, label_im, range(nb_labels + 1))
    masked_sizes = np.ma.masked_equal(sizes, 0.0, copy=False)
    sizes_sorted = np.copy(sizes)
    sizes_sorted.sort()
    n = 3 if len(sizes_sorted) >= 3 else len(sizes_sorted)
    for e in range(1, n + 1):
        ftr_size_biggest_objects.append(sizes_sorted[-1 * e])
    ftr_size_total = np.sum(sizes)

    try:
        # if True:
        r, c = np.vstack(ndimage.center_of_mass(img_array_2d, label_im, np.arange(nb_labels) + 1)).T
        properties = regionprops(label_im)
        if plot_mode:
            print('Label \tLargest side \tSmallest side')
        ftrs_objects = np.empty((len(properties), 3))
        e = 0
        for p in properties:
            min_row, min_col, max_row, max_col = p.bbox
            max_side = max(max_row - min_row, max_col - min_col)
            min_side = min(max_row - min_row, max_col - min_col)
            ftrs_objects[e, 0] = sizes[e + 1]
            ftrs_objects[e, 1] = max_side
            ftrs_objects[e, 2] = min_side
            e += 1
            if plot_mode:
                print('%5d %14.1f %14.1f' % (p.label, max_side, min_side))
        if plot_mode:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            ax.imshow(np.ma.masked_array(label_im), cmap=plt.cm.rainbow)
            ax.set_title('Labeled objects')
            plt.xticks([])
            plt.yticks([])
            for ri, ci, li in zip(r, c, range(1, nb_labels + 1)):
                ax.annotate(li, xy=(ci, ri), fontsize=12)
            plt.show()
        ftrs_objects = ftrs_objects[ftrs_objects[:, 0].argsort()]
        if len(ftrs_objects) >= 2:
            ftrs_object = ftrs_objects[-1]  ####### return biggest object features
            ftrs_object2 = ftrs_objects[-2]  ####### return 2nd biggest object features
        elif len(ftrs_objects) == 1:
            ftrs_object = ftrs_objects[-1]
            ftrs_object2 = np.full((3), 0.)
        else:
            ftrs_object = np.full((3), 0.)
            ftrs_object2 = np.full((3), 0.)
        return ftr_size_total, ftrs_object, ftrs_object2
    except: ### no objects found
        return ftr_size_total, np.full((3), 0.), np.full((3), 0.)


def save_img_to_dir(img_array_2d, dest, filename, cmap=None, vmin=0.0, vmax=1.0):
    if not os.path.exists(dest):
        os.makedirs(dest)
    if cmap is None:
        plt.imsave(fname=dest + filename, arr=img_array_2d, vmin=vmin, vmax=vmax)
    else:
        plt.imsave(fname=dest + filename, arr=img_array_2d, cmap=cmap, vmin=vmin, vmax=vmax)


def open_hdf5(filename):
    h5f = h5py.File(filename, 'r')
    np_array = h5f['dataset'][:]
    h5f.close()
    heatmap = np_array[::, ::, 0]
    return heatmap


def _mmap_h5(path, h5path):
    with h5py.File(path) as f:
        ds = f[h5path]
        # We get the dataset address in the HDF5 fiel.
        offset = ds.id.get_offset()
        # We ensure we have a non-compressed contiguous array.
        assert ds.chunks is None
        assert ds.compression is None
        assert offset > 0
        dtype = ds.dtype
        shape = ds.shape
    arr = np.memmap(path, mode='r', shape=shape, offset=offset, dtype=dtype)
    return arr


def get_max_heigth_width(input_dir, pad_mode=False):
    max_x, max_y = 0, 0
    min_x, min_y = 9999999, 9999999
    folders_done = 0
    imgs_done = 0

    for dirpath, dirnames, filenames in os.walk(input_dir):
        if folders_done % 10 == 0: print('folders / imgs processed: %d / %d' % (folders_done, imgs_done))
        for filename in [f for f in filenames]:

            try:
                f_name = os.path.join(dirpath, filename)
                # heatmap = open_hdf5(f_name)
                np_array = _mmap_h5(f_name, 'dataset')
                np_array = np_array[:]
                heatmap = np_array[::, ::, 0]
                if pad_mode:
                    heatmap = cut_off_zero_sides(heatmap)
                y_dim, x_dim = get_y_x_dims(heatmap)
                if pad_mode and x_dim > y_dim:
                    heatmap = np.rot90(heatmap)
                max_x, min_x, max_y, min_y = check_min_max_dims(heatmap, max_x, min_x, max_y, min_y)
                imgs_done += 1
            except Exception as ex:
                e = sys.exc_info()[0]
                print(e)
                print(ex)
                print(f_name)

        folders_done += 1
    print('folders scanned: \t%d:' % (folders_done))
    print('images processed: \t%d' % (imgs_done))
    return max_y, max_x


def threshold_heatmap(img_array_2d, threshold):
    thresholded = np.where(img_array_2d > threshold, 1., 0., )
    return thresholded


def open_close_heatmap(img_array_2d, opening_iter, closing_iter):
    if opening_iter != 0:
        ndimage.binary_opening(input=img_array_2d, iterations=opening_iter, output=img_array_2d)
    if closing_iter != 0:
        ndimage.binary_closing(input=img_array_2d, iterations=closing_iter, output=img_array_2d)
    return img_array_2d


def rotate_heatmap(img_array_2d):
    y_dim, x_dim = get_y_x_dims(img_array_2d)
    if x_dim > y_dim:
        img_array_2d = np.rot90(img_array_2d)
    return img_array_2d


def draw_xml_mask_on_heatmap_non_PIL(img_array_2d, xml_masks_dir, f_name):
    xml_name = f_name + '.xml'
    for dirpath2, dirnames2, filenames2 in os.walk(xml_masks_dir):
        for filename2 in [f for f in filenames2 if f.endswith(xml_name)]:
            f_name_xml = os.path.join(dirpath2, filename2)
            coords = parse_single_xml_mask(f_name_xml, 256)
            drawn_mask = np.full((img_array_2d.shape[0], img_array_2d.shape[1], 3), 0)
            mask_only = np.full((img_array_2d.shape[0], img_array_2d.shape[1]), 0)
            drawn_mask[:, :, 0] = img_array_2d
            drawn_mask[:, :, 1] = img_array_2d
            drawn_mask[:, :, 2] = img_array_2d
            for c in coords:
                drawn_mask[int(c[1]), int(c[0]), 0] = 1. #* drawn_mask[int(c[0]), int(c[1]), 0]
                drawn_mask[int(c[1]), int(c[0]), 1] = .0 #* drawn_mask[int(c[0]), int(c[1]), 1]
                drawn_mask[int(c[1]), int(c[0]), 2] = .0 #* drawn_mask[int(c[0]), int(c[1]), 2]
                mask_only[int(c[1]), int(c[0])] = 1. #* drawn_mask[int(c[0]), int(c[1]), 0]
            return drawn_mask, mask_only
    return None, None

def draw_xml_mask_on_heatmap(img_array_2d, xml_masks_dir, f_name):
    xml_name = f_name + '.xml'
    for dirpath2, dirnames2, filenames2 in os.walk(xml_masks_dir):
        for filename2 in [f for f in filenames2 if f.endswith(xml_name)]:
            f_name_xml = os.path.join(dirpath2, filename2)
            coords = parse_single_xml_mask(f_name_xml, 256)
            img = Image.new('L', (img_array_2d.shape[1], img_array_2d.shape[0]), 0)
            for c in coords:
                ImageDraw.Draw(img).polygon(c, outline=1, fill=0)
            mask_only = np.array(img)
            drawn_mask = np.full((img_array_2d.shape[0], img_array_2d.shape[1], 3), 0)
            drawn_mask[:, :, 0] = img_array_2d
            drawn_mask[:, :, 1] = img_array_2d
            drawn_mask[:, :, 2] = img_array_2d
            drawn_mask[:, :, 0] += mask_only
            drawn_mask[:, :, 1] -= mask_only
            drawn_mask[:, :, 2] -= mask_only
            return drawn_mask, mask_only
    return None, None


def process_heatmap(heatmap, output_dir, xml_masks_dir, f_name_out, threshold, opening_iter, closing_iter, save_pics):
    ### save heatmap
    destination = output_dir + '/01_heatmaps/'
    if save_pics: save_img_to_dir(heatmap, destination, f_name_out + '.png')
    ### save heatmap grayscale
    destination = output_dir + '/01a_heatmaps_gray/'
    if save_pics: save_img_to_dir(heatmap, destination, f_name_out + '.png', cmap=plt.cm.gist_gray)
    ### binarization
    thresholded = threshold_heatmap(heatmap, threshold)
    destination = output_dir + '/02_heatmaps_threshold/'
    if save_pics: save_img_to_dir(thresholded, destination, f_name_out + '.png', cmap=plt.cm.gist_gray)
    ### opening/closing
    open_closed = open_close_heatmap(thresholded, opening_iter, closing_iter)
    destination = output_dir + '/03_heatmaps_opening_closing/'
    if save_pics: save_img_to_dir(open_closed, destination, f_name_out + '.png', cmap=plt.cm.gist_gray)
    ### fill holes heatmap
    filled_holes = ndimage.morphology.binary_fill_holes(open_closed)
    destination = output_dir + '/04_heatmaps_filled_holes/'
    if save_pics: save_img_to_dir(filled_holes, destination, f_name_out + '.png', cmap=plt.cm.gist_gray)
    if xml_masks_dir is not None:
        ### get xml_mask polygons and draw into heatmap
        drawn_mask, mask_only = draw_xml_mask_on_heatmap(filled_holes, xml_masks_dir, f_name_out)
        if mask_only is not None: ### None if no xml found
            destination = output_dir + '/05_heatmaps_drawn_xml_mask/'
            if save_pics: save_img_to_dir(drawn_mask, destination, f_name_out + '.png', cmap=plt.cm.gist_gray)
            ### fill holes xml mask
            mask_only = ndimage.morphology.binary_fill_holes(mask_only)
            destination = output_dir + '/05a_xml_masks_filled_holes/'
            if save_pics: save_img_to_dir(mask_only, destination, f_name_out + '.png', cmap=plt.cm.gist_gray)
            ### subtract mask from filled_holes and get error
            error_mask = filled_holes - mask_only
            error = (float(error_mask.sum()) / float(mask_only.sum()))# ** 2
            destination = output_dir + '/06_heatmaps_error/'
            if save_pics: save_img_to_dir(error_mask, destination, f_name_out + '.png', cmap=plt.cm.gist_gray)
            return filled_holes, error
    return filled_holes, 0


def extract_features_all_heatmaps(input_dir, output_dir, xml_masks_dir=None,
                                  threshold=.5, closing_iter=6, opening_iter=0,
                                  pad_to_height=866, pad_to_width=434, pad_mode=False, save_pics=True):
    imgs_done = 0
    folders_done = 0
    ftr_sum_probs = []
    ftr_avg_probs = []
    ftr_highest_prob = []
    ftr_size_total = []
    ftrs_object = []
    ftrs_object2 = []
    data_name = []
    total_error = 0.0

    for dirpath, dirnames, filenames in os.walk(input_dir):
        if folders_done % 10 == 0: print('folders / imgs processed: %d / %d' % (folders_done, imgs_done))
        for filename in [f for f in filenames if f.endswith('')]:
            f_name = os.path.join(dirpath, filename)

            try:
            #if True:
                heatmap = open_hdf5(f_name)
                # np_array = _mmap_h5(f_name,'dataset')
                # np_array = np_array[:]
                # heatmap = np_array[::,::,0]

                f_name_out = dirpath.replace(input_dir, '')
                ### image processing
                if pad_mode:
                    heatmap = cut_off_zero_sides(heatmap)
                    heatmap = rotate_heatmap(heatmap)
                    heatmap = normalize_image_dims(heatmap, pad_to_width, pad_to_height)
                if pad_mode: xml_masks_dir = None
                processed_heatmap, error = process_heatmap(heatmap, output_dir, xml_masks_dir, f_name_out, threshold, opening_iter, closing_iter, save_pics)
                total_error += error
                ### feature extraction
                ftr1, ftr2, ftr3 = extract_features_on_raw_heatmap(heatmap)
                ftr_sum_probs.append(ftr1)
                ftr_avg_probs.append(ftr2)
                ftr_highest_prob.append(ftr3)
                ftr4, ftr5 , ftr6 = extract_features_on_closed_heatmap(processed_heatmap)
                ftr_size_total.append(ftr4)
                ftrs_object.append(ftr5)
                ftrs_object2.append(ftr6)
                data_name.append(f_name_out)

                imgs_done += 1

            except Exception as ex:
                e = sys.exc_info()[0]
                print(e)
                print(ex)
                print(f_name)

            folders_done += 1

    print('')
    print('========================================================')
    print('total error sum(((heatmap_i - xml_mask_i) / sum(xml_mask_i))): \t%d' % total_error)
    print('folders scanned: \t\t%d:' % (folders_done))
    print('images processed: \t\t%d' % (imgs_done))

    return data_name, ftr_sum_probs, ftr_avg_probs, ftr_highest_prob, ftr_size_total, ftrs_object, ftrs_object2