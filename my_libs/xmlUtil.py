import xml.etree.ElementTree as ET



def parse_single_xml_mask(input_file, zoom=256):
    """
    Parses one xml-mask from camelyon16 or camelyon17 challenge.

    Parameters
    ----------
    arg1 : str
        Path to the xml-file.

    arg2 : int
        Zoom-Factor. Coordinates will be divided by this.

    Returns
    -------
    list
        List containing other list, of which each defines a polygon.
    """
    tree = ET.parse(input_file)
    root = tree.getroot()
    total_coords= []

    for child in root:
        for child in child:
            for child in child:
                coords = []
                for coord in child.findall('Coordinate'):
                    x = coord.get('X')
                    y = coord.get('Y')
                    coords.append((float(x) // zoom, float(y) // zoom))
                if len(coords) >= 2:
                    total_coords.append(coords)
    return total_coords