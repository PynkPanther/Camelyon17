import pickle
import sys
from keras.objectives import categorical_crossentropy
from keras.metrics import categorical_accuracy as accuracy
from my_libs.mlUtil import *



def check_program_args(args):
    if len(args) != 6:
        print('This program must be started with 6 arguments in the following order:')
        print('(1): path to csv-file with features and labels.')
        print('\t 1st row column names')
        print('\t 1st column slide name')
        print('\t 2nd column labels')
        print('\t 3rd-nth column features')
        print('(2): output-directory')
        print('(3): maximum number of examples to use for training per label')
        print('(4): parameter settings for the classifier (0 or 1)')
        print('(5): features to use (column numbers of csv in arg#1, starting with 0 at slide name')



if __name__ == '__main__':

    check_program_args(sys.argv)

    csv_file = sys.argv[1]
    output_dir = sys.argv[2]
    n_labels = 4
    max_examples_to_use_per_label = int(sys.argv[3])
    param_settings = int(sys.argv[4])
    features_to_use = sys.argv[5]
    features_to_use = features_to_use.split('.')
    features_to_use = [int(f) for f in features_to_use]
    n_features = len(features_to_use)

    labels_to_use, max_examples_to_use = get_camelyon_labels_n_max_examples(n_labels, max_examples_to_use_per_label)
    n_folds = 10
    train_percentage = 1. - 1. / n_folds

    if param_settings == 0:
        n_filters_lin = [1024, 1024]
        dropout_lin = 0.5
        l_rate = 0.00001
        n_epochs = 3000
    if param_settings == 1:
        n_filters_lin = [2048, 2048]
        dropout_lin = 0.5
        l_rate = 0.00001
        n_epochs = 3000
    if param_settings == 2:
        n_filters_lin = [512, 512, 512]
        dropout_lin = 0.5
        l_rate = 0.00001
        n_epochs = 3000


    tf.reset_default_graph()
    sess = tf.Session()
    K.set_session(sess)
    img = tf.placeholder(tf.float32, shape=(None, n_features))
    labels = tf.placeholder(tf.float32, shape=(None, n_labels))
    outputs, preds = build_model_keras(img, [], [], [], [], 0, 0, 0., n_filters_lin, dropout_lin, n_labels, False, False)
    loss = tf.reduce_mean(categorical_crossentropy(labels, preds))
    train_step = tf.train.AdamOptimizer(l_rate).minimize(loss)
    acc_value = accuracy(labels, preds)
    net_size = np.sum([np.prod(v.get_shape().as_list()) for v in tf.trainable_variables()])
    print('network size: %d' % net_size)


    print('##########################################')
    print('##        Start Training / Testing      ##')
    print('##########################################')

    train_acc_avg = []
    test_acc_avg = []
    train_f1_avg = []
    test_f1_avg = []

    test_preds_all = []
    test_names_all = []

    for fold in range(n_folds + 1):

        if fold < (n_folds): ### Cross Validation
            x_train, x_test, y_train, y_test, names_train, names_test = get_feature_vectors_and_labels(train_percentage, fold, max_examples_to_use, csv_file, labels_to_use, features_to_use, one_hot=True)
            x_train, x_test, _ = normalize_vectors(x_train, x_test)
        elif fold == (n_folds): ### predict rest of Training set
            x_train, y_train, names_train, x_test, y_test, names_test = get_rest_of_ftr_vectors(max_examples_to_use,
                                                                                                csv_file, labels_to_use,
                                                                                                features_to_use, True)

        # Initialize all variables
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())

        epoch_accs_train = []
        epoch_accs_test = []
        epoch_cms_train = []
        epoch_cms_test = []
        epoch_f1scores_train = []
        epoch_f1scores_test = []
        eval_epochs = 10

        with sess.as_default():
            for e in range(n_epochs):
                if e > 0:
                    ############
                    ### training
                    for batch in range(2):
                        start = batch * len(x_train) // 2
                        end = (batch + 1) * len(x_train) // 2
                        _, = sess.run([train_step], feed_dict={img: x_train[start:end],
                                                               labels: y_train[start:end],
                                                               K.learning_phase(): 1})
                if e % eval_epochs == 0:
                    ############
                    ### eval trainingset
                    preds_train , acc_t = sess.run([preds, acc_value], feed_dict={img: x_train,
                                                                                 labels: y_train,
                                                                                 K.learning_phase(): 0})
                    preds_train = np.argmax(preds_train, 1)
                    labels_true = np.argmax(y_train,1)
                    #if fold < (n_folds): ### Cross Validation
                    f1scores = f1_score(labels_true, preds_train, average=None)
                    cm_train = confusion_matrix(labels_true, preds_train)
                    epoch_accs_train.append(acc_t.sum() / acc_t.size)
                    epoch_cms_train.append(cm_train)
                    epoch_f1scores_train.append(f1scores)
                    ############
                    ### eval testset
                    preds_test, acc_t = sess.run([preds, acc_value], feed_dict={img: x_test,
                                                                                labels: y_test,
                                                                                K.learning_phase(): 0})
                    preds_test = np.argmax(preds_test, 1)
                    labels_true = np.argmax(y_test,1)
                    #if fold < (n_folds): ### Cross Validation
                    f1scores = f1_score(labels_true, preds_test, average=None)
                    cm_test = confusion_matrix(labels_true, preds_test)
                    epoch_accs_test.append(acc_t.sum() / acc_t.size)
                    epoch_cms_test.append(cm_test)
                    epoch_f1scores_test.append(f1scores)
                    phase = 'cross_valid_fold_ ' + str(fold + 1) + ' / ' + str(n_folds)
                    print_progress(e, epoch_accs_train, epoch_accs_test, cm_train, cm_test, epoch_f1scores_train, epoch_f1scores_test, phase)

                if e == n_epochs - 1:
                    test_preds_all.extend(preds_test)
                    test_names_all.extend(names_test)

        if fold < (n_folds): ### Cross Validation
            train_acc_avg.append(epoch_accs_train)
            test_acc_avg.append(epoch_accs_test)
            train_f1_avg.append((epoch_f1scores_train))
            test_f1_avg.append(epoch_f1scores_test)

    train_acc_avg = np.mean(train_acc_avg, axis=0)
    test_acc_avg = np.mean(test_acc_avg, axis=0)
    train_f1_avg = np.mean(train_f1_avg, axis=0)
    test_f1_avg = np.mean(test_f1_avg, axis=0)
    print_average_results(train_acc_avg, test_acc_avg, train_f1_avg, test_f1_avg)

    with open(output_dir + '/results.pickle', 'wb') as f:
        pickle.dump([train_acc_avg, test_acc_avg, train_f1_avg, test_f1_avg], f)
    print('pickling done')

    test_preds_all, test_names_all = remove_duplicates(test_preds_all, test_names_all)

    write_predictions_to_file(pat_s=0, pat_e=100, f_name=output_dir + '/predicted_trainset.csv', preds=test_preds_all, preds_names=test_names_all, labels_to_use=labels_to_use)
