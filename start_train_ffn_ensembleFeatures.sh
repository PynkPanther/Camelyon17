#!/bin/bash

for s in 1
do
    for dir in ./output_dir/*/
    do
        #if [[ "$dir" == *paddedFalse* ]]; then
        if [[ "$dir" == *ensemble* ]]; then

            echo "$dir"

            ### Feel free experimenting ;)
            examples_to_use_per_label=9999
            ffn_settings=1 # currentyl only 0-2 supported (see train_ffn.py what these settings are)
            feature_idx='2.3.4.5.6.7.8.9.10.11.12.13.14.15.16.17.18.19.20.21.22.23.24.25' # features to use (2-11 are supported, see /output_dir/SOMEDIR/features.csv what these features are)

            ### DONT TOUCH
            main_working_dir="$dir"

            csv_labels="$main_working_dir"features.csv
            output_dir="$main_working_dir"ffn_settings"$ffn_settings"_examplesperlabel"$examples_to_use_per_label"_features"$feature_idx"/
            mkdir "$output_dir"
            log="$output_dir"/log.txt

            python3 train_ffn.py "$csv_labels" "$output_dir" "$examples_to_use_per_label" "$ffn_settings" "$feature_idx" 2>&1 | tee "$log"
            #python3 train_ffn.py "$csv_labels" "$output_dir" "$examples_to_use_per_label" "$ffn_settings" "$feature_idx" > "$log"

        fi
    done
done