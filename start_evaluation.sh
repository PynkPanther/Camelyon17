#!/bin/bash

tmp_value_filename='accTrainTest_f1TrainTest.txt'

for dir in ./output_dir/*/*/
#for dir in ./output_dir/*.90*/*/
do
    if [[ "$dir" == *settings* ]]; then
        echo "$dir"
        main_working_dir="$dir"/

        if [[ -s "$dir"/"$tmp_value_filename" ]]; then
            echo 'accTrainTest_f1TrainTest.txt file found'
        else
            echo 'accTrainTest_f1TrainTest.txt file not found'
            log="$main_working_dir""$tmp_value_filename"
            pickle_file="$main_working_dir"/results.pickle
            python3 evaluation.py "$main_working_dir" "$pickle_file" > "$log"
        fi
        if [[ -s "$dir"/kappa.txt ]]; then
            echo 'kappa.txt file found'
        else
            echo 'kappa.txt file not found'
            log="$main_working_dir"kappa.txt
            python3 ./input_dir/evaluate_python3.py --ground_truth ./input_dir/labels_c17_train.csv --submission "$main_working_dir"predicted_trainset.csv >"$log"
        fi

    fi
done


## declare an array variable
declare -a arr=("cnn" "ffn" "rf" "")

## now loop through the above array
for i in "${arr[@]}"
do

    summaryfile=./output_dir/final_summary_"$i".csv
    echo preprocessing_params,classification_params,kappa,acc_train,acc_valid,f1_l1_train,f1_l2_train,f1_l3_train,f1_l4_train,f1_l1_valid,f1_l2_valid,f1_l3_valid,f1_l4_valid,preprocessing_error > "$summaryfile"

    for dir in ./output_dir/*
    do
        if [[ ("$i" == 'cnn') ]]; then
            preprFolder=paddedTrue
        elif [[ ("$i" == '') ]]; then
            preprFolder=padded
        else
            preprFolder=paddedFalse
        fi

        if [[ "$dir" == *"$preprFolder"* ]]; then
            dirToEcho=${dir/.\/output_dir\//}

            preprError=0
            for dir2 in "$dir"/*
            do
                if [[ "$dir2" == *log.txt* ]]; then
                    preprError=$(cat $dir2)

                    readarray -t lines <<< "$preprError"
                    preprError=${lines[${#lines[@]}-3]}
                    preprError=$(echo $preprError| cut -d':' -f 2)
                    preprError=$(echo $preprError| cut -d' ' -f 2)
                    preprError=$(echo $preprError| cut -d' ' -f 2)
                fi
            done


            echo "$dirToEcho" >> "$summaryfile"
            for dir2 in "$dir"/*
            do

                if [[ "$dir2" == *"$i"_settings* ]]; then
                    dir2ToEcho=${dir2/"$dir"\//}
                    kappa='nan'
                    values='nan,nan,nan,nan,nan,nan,nan,nan,nan,nan'
                    for file in "$dir2"/*
                    do
                        if [[ "$file" == *kappa.txt* ]]; then
                            kappa=$( sed 4!d $file)
                            kappa=$(echo $kappa| cut -d' ' -f 2)
                            kappa=${kappa:0:5}
                        fi
                        if [[ "$file" == *"$tmp_value_filename"* ]]; then
                            values=$( cat "$file" )
                        fi
                    done
                    echo "$dirToEcho","$dir2ToEcho","$kappa","$values","$preprError" >> "$summaryfile"
                fi
            done

         fi
    done

done