import pickle
import sys
from keras.objectives import categorical_crossentropy
from keras.metrics import categorical_accuracy as accuracy
from my_libs.mlUtil import *



def check_program_args(args):
    if len(args) != 8:
        print('This program must be started with 8 arguments in the following order:')
        print('(1): path folder with images')
        print('(2): path to csv-file with image names and labels.')
        print('\t 1st row column names')
        print('\t 1st column slide name')
        print('\t 2nd column labels')
        print('\t 3rd-nth column features')
        print('(3): output-directory')
        print('(4): width of the images')
        print('(5): height of the images')
        print('(6): maximum number of examples to use for training per label')
        print('(7): parameter settings for the classifier (0,1 or 2)')



if __name__ == '__main__':

    check_program_args(sys.argv)

    dir_data = sys.argv[1]
    csv_labels = sys.argv[2]
    output_dir = sys.argv[3]
    width = int(sys.argv[4])
    height = int(sys.argv[5])
    n_labels = 4
    max_examples_to_use_per_label = int(sys.argv[6])
    param_settings = int(sys.argv[7])

    labels_to_use, max_examples_to_use = get_camelyon_labels_n_max_examples(n_labels, max_examples_to_use_per_label)

    n_channels = 1
    n_features = height * width * n_channels
    n_folds = 10
    train_percentage = 1. - 1. / n_folds

    if param_settings == 0: ### Simpliefied AlexNet#1
        n_filters_convs = [96, 128, 256, 256, 128] ### original [96, 256, 384, 384, 256]
        filter_sizes = [11, 5, 3, 3, 3]
        conv_strides = [4, 2, 1, 1, 1] ### not sure about this
        pooling = [1, 1, 0, 0, 1]
        pool_size = 3
        pool_stride = 2
        droput_conv = 0.5
        n_filters_lin = [1024, 1024] ### [original 2048, 2048]
        dropout_lin = 0.5
        batch_size = 20
        l_rate = 0.00001
        n_epochs = 2000
        cyclic_mode = False
        roll_dense = False
    if param_settings == 1: ### Simplified AlexNet#2
        n_filters_convs = [96, 128, 128, 128, 128]
        filter_sizes = [11, 5, 5, 3, 3]
        conv_strides = [4, 2, 1, 1, 1]
        pooling = [1, 1, 0, 0, 1]
        pool_size = 3
        pool_stride = 2
        droput_conv = 0.5
        n_filters_lin = [512, 512]
        dropout_lin = 0.5
        batch_size = 20
        l_rate = 0.00001
        n_epochs = 2000
        cyclic_mode = False
        roll_dense = False
    if param_settings == 2: ### Simplified VGGNet-13 Style
        n_filters_convs = [64, 64, 64, 96, 96, 96, 96, 128, 128, 128, 128, 128]
        filter_sizes = [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3]
        conv_strides = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        pooling = [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1]
        pool_size = 2
        pool_stride = 2
        droput_conv = 0.5
        n_filters_lin = [1024, 1024] ### originally [4096, 4096]
        dropout_lin = 0.5
        batch_size = 5
        l_rate = 0.00001
        n_epochs = 1000
        cyclic_mode = False
        roll_dense = False
    if param_settings == 3: ### SimpleNet https://arxiv.org/pdf/1608.06037.pdf
        n_filters_convs = [64, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128] ### originally [512, 512] in the last 2
        filter_sizes = [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 3]
        conv_strides = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        pooling = [0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1] ### or (not clear): [0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1]
        pool_size = 2
        pool_stride = 2
        droput_conv = 0.5
        n_filters_lin = []
        dropout_lin = 0.5
        batch_size = 3
        l_rate = 0.00001
        n_epochs = 1000
        cyclic_mode = False
        roll_dense = False
    if param_settings == 4: ### Simpliefied AlexNet#3 no dense layer
        n_filters_convs = [96, 128, 256, 256, 128] ### original [96, 256, 384, 384, 256]
        filter_sizes = [11, 5, 3, 3, 3]
        conv_strides = [4, 2, 1, 1, 1] ### not sure about this
        pooling = [1, 1, 0, 0, 1]
        pool_size = 3
        pool_stride = 2
        droput_conv = 0.5
        n_filters_lin = [] ### [original 2048, 2048]
        dropout_lin = 0.5
        batch_size = 20
        l_rate = 0.00001
        n_epochs = 2000
        cyclic_mode = False
        roll_dense = False
    if param_settings == 5: ### Simpliefied AlexNet#1
        n_filters_convs = [96, 128, 128, 128, 128] ### original [96, 256, 384, 384, 256]
        filter_sizes = [11, 5, 3, 3, 3]
        conv_strides = [4, 2, 1, 1, 1] ### not sure about this
        pooling = [1, 1, 0, 0, 1]
        pool_size = 3
        pool_stride = 2
        droput_conv = 0.5
        n_filters_lin = [] ### [original 2048, 2048]
        dropout_lin = 0.5
        batch_size = 20
        l_rate = 0.00001
        n_epochs = 2000
        cyclic_mode = False
        roll_dense = False
    if param_settings == 6: ### Simpliefied AlexNet#1
        n_filters_convs = [48, 64, 64, 64, 64] ### original [96, 256, 384, 384, 256]
        filter_sizes = [11, 5, 3, 3, 3]
        conv_strides = [4, 2, 1, 1, 1] ### not sure about this
        pooling = [1, 1, 0, 0, 1]
        pool_size = 3
        pool_stride = 2
        droput_conv = 0.5
        n_filters_lin = [] ### [original 2048, 2048]
        dropout_lin = 0.5
        batch_size = 20
        l_rate = 0.00001
        n_epochs = 2000
        cyclic_mode = False
        roll_dense = False
    if param_settings == 7: ### Simpliefied AlexNet#1
        n_filters_convs = [32, 48, 48, 48, 48] ### original [96, 256, 384, 384, 256]
        filter_sizes = [11, 5, 3, 3, 3]
        conv_strides = [4, 2, 1, 1, 1] ### not sure about this
        pooling = [1, 1, 0, 0, 1]
        pool_size = 3
        pool_stride = 2
        droput_conv = 0.5
        n_filters_lin = [] ### [original 2048, 2048]
        dropout_lin = 0.5
        batch_size = 20
        l_rate = 0.00001
        n_epochs = 2000
        cyclic_mode = False
        roll_dense = False
    if param_settings == 8:  ### Simpliefied AlexNet#1
        n_filters_convs = [32, 48, 48, 48, 48]  ### original [96, 256, 384, 384, 256]
        filter_sizes = [11, 5, 3, 3, 3]
        conv_strides = [4, 2, 1, 1, 1]  ### not sure about this
        pooling = [1, 1, 0, 0, 1]
        pool_size = 3
        pool_stride = 2
        droput_conv = 0.5
        n_filters_lin = []  ### [original 2048, 2048]
        dropout_lin = 0.5
        batch_size = 20
        l_rate = 0.00001
        n_epochs = 2000
        cyclic_mode = True
        roll_dense = False
    if param_settings == 9:  ### Simpliefied AlexNet#1
        n_filters_convs = [48, 64, 64, 64, 64]  ### original [96, 256, 384, 384, 256]
        filter_sizes = [11, 5, 3, 3, 3]
        conv_strides = [4, 2, 1, 1, 1]  ### not sure about this
        pooling = [1, 1, 0, 0, 1]
        pool_size = 3
        pool_stride = 2
        droput_conv = 0.5
        n_filters_lin = []  ### [original 2048, 2048]
        dropout_lin = 0.5
        batch_size = 20
        l_rate = 0.00001
        n_epochs = 2000
        cyclic_mode = True
        roll_dense = False


    tf.reset_default_graph()

    sess = tf.Session()
    K.set_session(sess)

    img = tf.placeholder(tf.float32, shape=(batch_size, n_features))
    img_tensor = tf.reshape(img, [batch_size, height, width, n_channels])
    labels = tf.placeholder(tf.float32, shape=(None, n_labels))

    outputs, preds = build_model_keras(img_tensor, n_filters_convs, filter_sizes,
                                       conv_strides, pooling, pool_size, pool_stride,
                                       droput_conv, n_filters_lin, dropout_lin, n_labels,
                                       cyclic_mode, roll_dense)
    loss = tf.reduce_mean(categorical_crossentropy(labels, preds))
    train_step = tf.train.AdamOptimizer(l_rate).minimize(loss)
    acc_value = accuracy(labels, preds)

    net_size = np.sum([np.prod(v.get_shape().as_list()) for v in tf.trainable_variables()])
    print('network size: %d' % net_size)


    print('##########################################')
    print('##        Start Training / Testing      ##')
    print('##########################################')

    train_acc_avg = []
    test_acc_avg = []
    train_f1_avg = []
    test_f1_avg = []

    test_preds_all = []
    test_names_all = []

    for fold in range(n_folds + 1):

        if fold < (n_folds): ### Cross Validation
            filenames_train, filenames_test, labels_train, labels_test = get_camelyon_train_test_split_cnn(
                dir_data, train_percentage, fold, max_examples_to_use, csv_labels, labels_to_use)
        elif fold == (n_folds): ### predict rest of Training set
            filenames_train, _, labels_train, _ = get_camelyon_train_test_split_cnn(
                dir_data, 1.0, fold, max_examples_to_use, csv_labels, labels_to_use)
            for l in range(len(max_examples_to_use)):
                max_examples_to_use[l] = max_examples_to_use[l] * 999999
            filenames_test, _, labels_test, _ = get_camelyon_train_test_split_cnn(
                dir_data, 1.0, fold, max_examples_to_use, csv_labels, labels_to_use)

            labels_test, filenames_test = remove_samples_from_set(labels_test, filenames_test, filenames_train)

        if len(labels_test) == 0:
            break

        train_data, train_labels, train_names = get_queue_batches_cnn(filenames_train, labels_train, height, width,
                                                                      n_channels,
                                                                      n_labels,
                                                                      batch_size)
        test_data, test_labels, test_names = get_queue_batches_cnn(filenames_test, labels_test, height, width,
                                                                   n_channels, n_labels,
                                                                   batch_size)




        # Initialize all variables
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())

        # Coordinate the loading of image files.
        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(sess=sess, coord=coord)

        epoch_accs_train = []
        epoch_accs_test = []
        epoch_cms_train = []
        epoch_cms_test = []
        epoch_f1scores_train = []
        epoch_f1scores_test = []
        eval_epochs = 10

        with sess.as_default():
            for e in range(n_epochs):
                if e > 0:
                    ############
                    ### training
                    processed_imgs = 0
                    while processed_imgs < len(filenames_train):
                        imgs_fetched, labels_fetched = sess.run([train_data, train_labels])
                        norm_batch = imgs_fetched
                        _, = sess.run([train_step], feed_dict={img: norm_batch,
                                                               labels: labels_fetched,
                                                               K.learning_phase(): 1})
                        processed_imgs += batch_size

                if e % eval_epochs == 0:
                    f1scores_train_ill_defined = True
                    f1scores_test_ill_defined = True
                    trys = 0
                    while (f1scores_train_ill_defined or f1scores_test_ill_defined) and trys < 3:
                        print('___DEBUG: epoch - ' + str(e) + ' - evaluation try: ' + str(trys))
                        ############
                        ### eval trainingset
                        accs_train, cm_train, f1scores_train, _, _, f1scores_train_ill_defined = eval_split_cnn(preds, acc_value, img, labels, batch_size, sess, train_data, train_labels, train_names, len(filenames_train))
                        ############
                        ### eval testset
                        accs_test, cm_test, f1scores_test, preds_test, names_test, f1scores_test_ill_defined = eval_split_cnn(preds, acc_value, img, labels, batch_size, sess, test_data, test_labels, test_names, len(filenames_test))

                        trys += 1

                        ### workaround: in 1 of 100 tests F1 is ill defined due to small batch sizes (e.g. class itc not present)... better to not track any quality measures then
                    if f1scores_train_ill_defined == False and f1scores_test_ill_defined == False:
                        epoch_f1scores_train.append(f1scores_train)
                        epoch_accs_train.append(np.mean(accs_train))
                        epoch_cms_train.append(cm_train)
                        epoch_f1scores_test.append(f1scores_test)
                        epoch_accs_test.append(np.mean(accs_test))
                        epoch_cms_test.append(cm_test)

                        phase = 'cross_valid_fold_ ' + str(fold + 1) + ' / ' + str(n_folds)
                        print_progress(e, epoch_accs_train, epoch_accs_test, cm_train, cm_test, epoch_f1scores_train, epoch_f1scores_test, phase)

                if e == n_epochs - 1:
                    test_preds_all.extend(preds_test)
                    test_names_all.extend(names_test)

        if fold < (n_folds): ### Cross Validation
            train_acc_avg.append(epoch_accs_train)
            test_acc_avg.append(epoch_accs_test)
            train_f1_avg.append((epoch_f1scores_train))
            test_f1_avg.append(epoch_f1scores_test)

    # When done, ask the threads to stop.
    coord.request_stop()
    # And wait for them to actually do it.
    coord.join(threads)

    train_acc_avg = np.mean(train_acc_avg, axis=0)
    test_acc_avg = np.mean(test_acc_avg, axis=0)
    train_f1_avg = np.mean(train_f1_avg, axis=0)
    test_f1_avg = np.mean(test_f1_avg, axis=0)
    print_average_results(train_acc_avg, test_acc_avg, train_f1_avg, test_f1_avg)


    with open(output_dir + '/results.pickle', 'wb') as f:
        pickle.dump([train_acc_avg, test_acc_avg, train_f1_avg, test_f1_avg], f)
    print('pickling done')



    for e in range(len(test_preds_all)):
        name_only = str(test_names_all[e]).split('/')
        name_only = name_only[-1]
        name_only = name_only.replace('.png','')
        test_names_all[e] = name_only

    test_preds_all, test_names_all = remove_duplicates(test_preds_all, test_names_all)

    write_predictions_to_file(pat_s=0, pat_e=100, f_name=output_dir + '/predicted_trainset.csv', preds=test_preds_all, preds_names=test_names_all, labels_to_use=labels_to_use)
