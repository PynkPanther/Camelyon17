import sys
import pickle
from sklearn.ensemble import RandomForestClassifier
from my_libs.mlUtil import *


def check_program_args(args):
    if len(args) != 6:
        print('This program must be started with 6 arguments in the following order:')
        print('(1): path to csv-file with features and labels.')
        print('\t 1st row column names')
        print('\t 1st column slide name')
        print('\t 2nd column labels')
        print('\t 3rd-nth column features')
        print('(2): output-directory')
        print('(3): maximum number of examples to use for training per label')
        print('(4): parameter settings for the classifier (0 or 1)')
        print('(5): features to use (column numbers of csv in arg#1, starting with 0 at slide name')


if __name__ == '__main__':

    check_program_args(sys.argv)

    csv_file = sys.argv[1]
    output_dir = sys.argv[2]
    n_labels = 4
    max_examples_to_use_per_label =int(sys.argv[3])
    param_settings = int(sys.argv[4])
    features_to_use = sys.argv[5]
    features_to_use = features_to_use.split('.')
    features_to_use = [int(f) for f in features_to_use]


    labels_to_use, max_examples_to_use = get_camelyon_labels_n_max_examples(n_labels, max_examples_to_use_per_label)
    n_folds = 10
    train_percentage = 1. - 1. / n_folds

    seed = 0
    if param_settings == 0:
        rf_depth = None #10
        rf_nb_trees = 300
        criterion = 'gini'
    if param_settings == 1:
        rf_depth = None #10
        rf_nb_trees = 400
        criterion = 'gini'
    if param_settings == 2:
        rf_depth = None #10
        rf_nb_trees = 500
        criterion = 'gini'
    if param_settings == 3:
        rf_depth = None #10
        rf_nb_trees = 1000
        criterion = 'gini'
    if param_settings == 4:
        rf_depth = 5  # 10
        rf_nb_trees = 300
        criterion = 'gini'
    if param_settings == 5:
        rf_depth = 5  # 10
        rf_nb_trees = 400
        criterion = 'gini'
    if param_settings == 6:
        rf_depth = 5  # 10
        rf_nb_trees = 500
        criterion = 'gini'
    if param_settings == 7: ### best
        rf_depth = 5  # 10
        rf_nb_trees = 1000
        criterion = 'gini'


    ### 7 playing nb_trees
    if param_settings == 8:
        rf_depth = 5  # 10
        rf_nb_trees = 800
        criterion = 'gini'
    if param_settings == 9: ### best
        rf_depth = 5  # 10
        rf_nb_trees = 1500
        criterion = 'gini'
    if param_settings == 10:
        rf_depth = 5  # 10
        rf_nb_trees = 2000
        criterion = 'gini'


    ### 9 playing_depth
    if param_settings == 11:
        rf_depth = 4  # 10
        rf_nb_trees = 1500
        criterion = 'gini'
    if param_settings == 12:
        rf_depth = 6  # 10
        rf_nb_trees = 1500
        criterion = 'gini'


    ### 12 trying entropy-criterion
    if param_settings == 13:
        rf_depth = 4  # 10
        rf_nb_trees = 1500
        criterion = 'entropy'



    train_acc_avg = 0
    test_acc_avg = 0
    train_f1_avg = np.full((n_labels),0,dtype=np.float32)
    test_f1_avg = np.full((n_labels),0,dtype=np.float32)

    test_preds_all = []
    test_names_all = []

    for fold in range(n_folds):
        x_train, x_test, y_train, y_test, names_train, names_test = get_feature_vectors_and_labels(train_percentage, fold, max_examples_to_use, csv_file, labels_to_use, features_to_use, one_hot=False)
        x_train, x_test, _ = normalize_vectors(x_train, x_test)

        #if example_mining:
        #    x_train, y_train, names_train = remove_bad_training_samples(x_train, y_train, names_train)


        random_forest = RandomForestClassifier(n_estimators=rf_nb_trees, max_depth=rf_depth, random_state=seed, criterion=criterion)
        random_forest.fit(x_train, y_train)

        ##### Fit rest of the Training set... unfortunatly does not work good
        #max_examples_to_use2 = [i * 99999 for i in max_examples_to_use]
        #x_rest, _, y_rest, _, names_rest, _ = get_feature_vectors_and_labels(1.0, 0, max_examples_to_use2, csv_file, labels_to_use, features_to_use, one_hot=False)
        #x_train, x_test, x_rest = normalize_vectors(x_train, x_test, x_rest)
        #x_rest, _ = remove_samples_from_set(x_rest, names_rest.copy(), names_train)
        #y_rest, names_rest = remove_samples_from_set(y_rest, names_rest, names_train)
        #x_rest, _ = remove_samples_from_set(x_rest, names_rest.copy(), names_test)
        #y_rest, names_rest = remove_samples_from_set(y_rest, names_rest, names_test)
        #random_forest.n_estimators += (rf_nb_trees)
        #random_forest.fit(x_rest, y_rest)

        acc_test, cm_test, f1_test, preds_test = eval_rf(x_test, y_test, random_forest)
        acc_train, cm_train, f1_train, preds_train= eval_rf(x_train, y_train, random_forest)
        test_preds_all.extend(preds_test)
        test_names_all.extend(names_test)

        train_acc_avg += acc_train
        test_acc_avg += acc_test
        train_f1_avg += f1_train
        test_f1_avg += f1_test

        phase = 'cross_valid_fold_ '+str(fold + 1) + ' / ' + str(n_folds)
        print_progress(0, [acc_train], [acc_test], cm_train, cm_test, [f1_train], [f1_test], phase)

    train_acc_avg = train_acc_avg / n_folds
    test_acc_avg = test_acc_avg / n_folds
    train_f1_avg = train_f1_avg / n_folds
    test_f1_avg = test_f1_avg / n_folds
    print_average_results(train_acc_avg, test_acc_avg, train_f1_avg, test_f1_avg)

    with open(output_dir + '/results.pickle', 'wb') as f:
        pickle.dump([[train_acc_avg], [test_acc_avg], [train_f1_avg], [test_f1_avg]], f)
    print('pickling done')


    ########################################################################################
    ##       Evaluating Rest of the Training set which was not in Crossvaldiation         ##
    ########################################################################################
    x_train, y_train, names_train, x_rest, y_rest, names_rest = get_rest_of_ftr_vectors(max_examples_to_use, csv_file, labels_to_use, features_to_use, False)
    #if example_mining:
    #    x_train, y_train, names_train = remove_bad_training_samples(x_train, y_train, names_train)

    random_forest = RandomForestClassifier(n_estimators=rf_nb_trees, max_depth=rf_depth, random_state=seed, criterion=criterion)
    random_forest.fit(x_train, y_train)
    if len(x_rest) > 0:
        acc_rest, cm_rest, f1_rest, preds_rest = eval_rf(x_rest, y_rest, random_forest)
        acc_train, cm_train, f1_train, preds_train = eval_rf(x_train, y_train, random_forest)
        print(f1_rest)
        print_progress(0, [acc_train], [acc_rest], cm_train, cm_rest, [f1_train], [f1_rest], 'predicting rest of the set')

        test_preds_all.extend(preds_rest)
        test_names_all.extend(names_rest)

    test_preds_all, test_names_all = remove_duplicates(test_preds_all, test_names_all)

    write_predictions_to_file(pat_s=0, pat_e=100, f_name=output_dir + '/predicted_trainset.csv', preds=test_preds_all, preds_names=test_names_all, labels_to_use=labels_to_use)


    ########################################################################################
    ##       Evaluating Rest of the TEST set                                              ##
    ########################################################################################
    max_examples_to_use.append(max_examples_to_use_per_label)
    max_examples_to_use2 = [i * 999999 for i in max_examples_to_use]
    x_train, _, y_train, _, names_train, _ = get_feature_vectors_and_labels(1.0, fold,
                                                                       max_examples_to_use,
                                                                       csv_file, labels_to_use,
                                                                       features_to_use,
                                                                       one_hot=False)
    labels_to_use.append('c17_test_unknown')
    x_testset, _, y_testset, _, names_testset, _ = get_feature_vectors_and_labels(1.0, 0,
                                                                           max_examples_to_use2,
                                                                           csv_file, [labels_to_use[-1]],
                                                                           features_to_use,
                                                                           one_hot=False)
    x_train, x_testset, _ = normalize_vectors(x_train, x_testset)
    #if example_mining:
    #    x_train, y_train, names_train = remove_bad_training_samples(x_train, y_train, names_train)

    random_forest = RandomForestClassifier(n_estimators=rf_nb_trees, max_depth=rf_depth, random_state=seed, criterion = criterion)
    random_forest.fit(x_train, y_train)
    preds_testset = random_forest.predict(x_testset)

    names_testset = names_testset.tolist()
    preds_testset, names_testset = remove_duplicates(preds_testset, names_testset)

    write_predictions_to_file(pat_s=100, pat_e=200, f_name=output_dir + '/predicted_testset.csv', preds=preds_testset, preds_names=names_testset, labels_to_use=labels_to_use[:-1])
